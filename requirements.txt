fastapi~=0.73.0
starlette~=0.17.1
pydantic~=1.9.0
uvicorn~=0.17.4
passlib~=1.7.4
bcrypt~=3.2.0