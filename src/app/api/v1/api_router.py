from fastapi import APIRouter

from src.app.api.v1.endpoints import auth, test

router = APIRouter()
router.include_router(auth.router, tags=["auth"])
router.include_router(test.router, prefix="/test", tags=["test"])
