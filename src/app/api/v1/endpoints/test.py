from fastapi import Depends, APIRouter

from src.auth.auth import get_current_active_user
from src.app.models.user import User
from src.common.decorators import role_check

router = APIRouter()


@router.get("/users/me/", response_model=User)
@role_check(roles=["admin"])
def read_users_me(current_user: User = Depends(get_current_active_user)):
    return current_user


@router.get("/users/me/items/")
@role_check(roles=["qdmin"])
def read_own_items(current_user: User = Depends(get_current_active_user)):
    return [{"item_id": "Foo", "owner": current_user.username}]
