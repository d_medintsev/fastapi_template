import functools
import inspect

from fastapi import HTTPException


def role_check(roles):
    def actual_decorator(func):
        @functools.wraps(func)
        async def wrap_func(*args, **kwargs):
            try:
                if inspect.iscoroutinefunction(func):
                    if kwargs['current_user'].role in roles:
                        result = await func(*args, **kwargs)
                    else:
                        raise HTTPException(status_code=400, detail="No access")
                else:
                    if kwargs['current_user'].role in roles:
                        result = func(*args, **kwargs)
                    else:
                        raise HTTPException(status_code=400, detail="No access")
            except HTTPException as e:
                raise e
            return result

        return wrap_func

    return actual_decorator
